(function() {
  "use strict";
  var agenda = [
    {
      id: 1,
      name: "Admin"
    },
    {
      id: 2,
      name: "Viber"
    },
    {
      id: 3,
      name: "me"
    },
    {
      id: 4,
      name: "eno"
    },
    {
      id: 5,
      name: "prigovor"
    },
    {
      id: 6,
      name: "lksdj"
    },
    {
      id: 7,
      name: "logo"
    }
  ];
  var upoloadDrop = new Dropzone("div#upload-drop", {
    url: "/upload",
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 1, // MB
    createImageThumbnails: false,
    previewsContainer: false,
    uploadMultiple: true,
    accept: function(file, done) {
      checkAgenda(file);
      done();
    }
  });
  createAgenda(agenda);
  upoloadDrop.on("queuecomplete", function(file) {
    console.log("done");
    console.log(agenda);
    updateAgenda(agenda);
  });
  function checkAgenda(file) {
    var counter = 0;
    agenda.forEach(function(element) {
      if (file.name.toLowerCase().includes(element.name.toLowerCase())) {
        if (element.file != undefined) {
          element.file.push(file.name);
        } else {
          element.file = [file.name];
        }
      } else {
        counter++;
      }
    }, this);
    if (counter === agenda.length) {
      console.log(
        "There is no agenda item that matches that file name: ",
        file.name
      );
    }
  }

  function createAgenda(agenda) {
    var agendaContainer = document.getElementById("agenda-container");
    for (var item in agenda) {
      if (agenda.hasOwnProperty(item)) {
        var element = agenda[item];

        var gridElement = document.createElement("div");
        gridElement.className = "mdl-grid";

        var idElement = document.createElement("div");
        var nameElement = document.createElement("div");
        var docElement = document.createElement("div");
        docElement.id = element.id;

        idElement.className = "mdl-cell mdl-cell--4-col";
        nameElement.className = "mdl-cell mdl-cell--4-col";
        docElement.className = "mdl-cell mdl-cell--4-col";
        idElement.innerHTML = "<p>" + element.id + "</p>";
        nameElement.innerHTML = "<p>" + element.name + "</p>";

        gridElement.appendChild(idElement);
        gridElement.appendChild(nameElement);
        gridElement.appendChild(docElement);
        agendaContainer.appendChild(gridElement);
      }
    }
  }

  function updateAgenda(agenda) {
    var agendaContainer = document.getElementById("agenda-container");
    for (var item in agenda) {
      if (agenda.hasOwnProperty(item)) {
        var element = agenda[item];
        if (element.file != undefined) {
          var concatFile = "<p>";
          element.file.forEach(function(uploadedFile) {
            concatFile += uploadedFile + "<br>";
          }, this);
          concatFile += "</p>";
          document.getElementById(element.id).innerHTML = concatFile;
        }
      }
    }
  }
  var minSteps = 6,
    maxSteps = 60,
    timeBetweenSteps = 100,
    bytesPerStep = 100000;

  upoloadDrop.uploadFiles = function(files) {
    var self = this;

    for (var i = 0; i < files.length; i++) {
      var file = files[i];
      var totalSteps = Math.round(
        Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep))
      );

      for (var step = 0; step < totalSteps; step++) {
        var duration = timeBetweenSteps * (step + 1);
        setTimeout(
          (function(file, totalSteps, step) {
            return function() {
              file.upload = {
                progress: 100 * (step + 1) / totalSteps,
                total: file.size,
                bytesSent: (step + 1) * file.size / totalSteps
              };

              self.emit(
                "uploadprogress",
                file,
                file.upload.progress,
                file.upload.bytesSent
              );
              if (file.upload.progress == 100) {
                file.status = Dropzone.SUCCESS;
                self.emit("success", file, "success", null);
                self.emit("complete", file);
                self.processQueue();
              }
            };
          })(file, totalSteps, step),
          duration
        );
      }
    }
  };
})();
